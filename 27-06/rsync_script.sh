#!/bin/bash


#Clareo la plantalla 
clear

source ./funciones.sh


# -------------------------------------------------------------------------------------------
echo ""
echo "///////////////////////////////////////////////////////////////////"
echo "| ----------------------------------------------------------------|"
echo "|                    *Bienvenido al sincronizador de              |"
echo "|                	            directorios*                        |"
echo "|                     			                        |"
echo "|-----------------------------------------------------------------|"
echo "|                                                                 |"
echo "| 1) Manual de instrucciones                                      |"
echo "| 								|"
echo "| 2)Copiar llave publica al host remoto (localhost)		|"
echo "|									|"
echo "| 3)Definimos entradas 'crontab' para automatizar el              |"
echo "|	 proceso de backup.               				|"
echo "|                                                                 |"
echo "| 4) Definimos el directorio de origen/destino que queremos       |" 
echo "|  sincronizar y ejecutamos el proceso de backup                  |"
echo "|                                                                 |"
echo "| 5) Salir                                                 	|"
echo "|                                                                 |"
echo "|               ----------------------------------                |"
echo "|               ----------------------------------                |"
echo "///////////////////////////////////////////////////////////////////"
echo ""

read -p "Indique una opción:" opcion

case $opcion in
	
	1)   func_ayuda
	
	
		;;
	
	
	2)     func_publickey   

		;;	
	
	
	3)
		echo "##########################################################################"
		echo "### Definimos entradas 'crontab' para automatizar el proceso de backup ###"
		echo "##########################################################################"
		echo ""

		#Preguntamos para saber si vamos a definir la fecha/hora/día de la ejecución del directorio
		echo "Desea programar la ejecución del script en una fecha,hora y minuto determinado?:Y/N:"
		read resp

		if [[ "$resp" == [yY] ]]; then

 
			crontab -e
			echo "Se programo la ejecución de la tarea correctamente."
			exit 0
		

		else
        		echo "Usted no desea automatizar la ejecución del proceso de backup, lo entendemos."
        		exit 0

		fi
		;;
	4)

	
		echo "##########################################################################"
                echo "###Definimos el directorio de origen y destino que queremos sincronizar ##"
                echo "##########################################################################"
                echo ""

		#Preguntamos para saber cual va a ser el directorio de origen que vamos a utilizar
		read -e -p"Ingresar directorio o archivo de origen:" origen
		
		#Preguntamos para saber cual va a ser el directorio de destino en donde se va a almacenar
	       	read -e -p "Ingresar directorio o archivo de destino (puede ser a un equipo remoto. Ejemplo: usuario@ip:/ruta/destino/):" destino


		echo "¿El origen $origen es correcto? Y/N:"
		read respuesta

		if [[ "$respuesta" == [yY] ]]; then

			
			echo "Origen confirmado"

		else
			
			echo "Modificar el Origen"
			exit 0
		fi
		
		
		echo "¿El destino $destino es correcto? Y/N:"
		read respuesta2

		if [[ "$respuesta" == [yY] ]]; then 

    			
    			echo "Destino confirmado"
		else
   
			echo "Modificar el Destino"
   			exit 0

		fi
		
		
		# Llamamos a la función de rsync
		func_rsync
				
		;;


	
	

	5) 	echo ""
   		echo "-------------------------"
   		echo "Saliendo del programa    "
   		echo "-------------------------"
   		echo ""
   		read -p "Aprete cualquier tecla para continuar:"
                exit 0
               
    		;;
	
	
	*) 	echo "Usted debe seleccionar alguna de las opciones validas del menu"
		
		;;
esac

