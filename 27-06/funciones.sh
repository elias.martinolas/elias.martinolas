#!/bin/bash

#Declaración de funciones

func_rsync() {


		echo "#######################"
                echo "###Backup Directorio ##"
                echo "#######################"
                echo ""


                local cancel="n"
                local ori="y"
                local dest="y"

                while [ $cancel == "n" ]; do

                        if [ $ori == "y" ] && [ $dest == "y" ]; then

                                echo "¿Iniciar el proceso? Y/N:"
                                read procesar

                                if [[ "$procesar" == [yY] ]]; then

                                         rsync --progress -avz "$origen" "$destino"
                                         if [ $? -ne 0 ]; then
                                                echo "########################################"
                                                echo "# Proceso de rsync terminó con errores #"
                                                echo "########################################"
                                                return 1
                                         else
                                                 echo "########################################"
                                                 echo "# Proceso rsync terminó correctamente  #"
                                                 echo "########################################"

                                                 return 0

                                         fi
                                else
                                        echo "¿Desea cancelar el proceso? Y/N:"
                                        read cancelar

                                        if [[ "$cancelar" == [yY] ]]; then

                                                echo "Proceso Cancelado"
                                                return 0
                                        fi

                                fi

                        else
                                echo "Proceso Cancelado. Origen o destino no existen"
                                return 0

                        fi

                done
}



func_publickey(){

                read -p "Ingrese su usuario:" user
		read -p "Ingrese el hostname que desea conectarse:" hostname
		#Comando para generar el par de llaves	
                #ssh-keygen -t rsa
		#Comando para realizar un copy al hostremoto(en este caso localhost) de la llave publica.
		ssh-copy-id -i ~/.ssh/id_rsa.pub ${user}@${hostname}

                if [[ $? -eq 0 ]]; then

                        echo "La llave publica fue compartida con el equipo remoto,"
                        
                        return 0
                else
                       echo "La llave publica no fue compartida con el equipo remoto, por favor intente nuevamnete."
                       return 1
                fi
}

func_ayuda() {

instrucciones="
                Opcion 2) Si usted elige la opción 2 va a copiar su llave publica y se la va a compartir a un hostremoto.


                Opcion 3) Si usted elige la opción 3 y acepta programar la fecha de la ejecución del proceso
                          de backup se abriba una pantalla en donde debe colocar la  siguente sentencia:

                * * * * *  / GUIÓN PARA EJECUTAR

                │ │ │ │ │
                │ │ │ │ │
                │ │ │ │ | _________   Día de la semana (0 - 6) (0 es domingo, o use nombres)
                │ │ │ |____________ Mes (1 - 12), * significa todos los meses
                │ │ |______________  Día del mes (1 - 31), * significa todos los días
                │ |________________  Hora (0 - 23), * significa cada hora
                |___________________ Minuto (0 - 59), * significa cada minuto

                - El asterisco (*) se utiliza para hacer coincidir todos los valores posibles de un campo.
                Por ejemplo, un asterisco usado en el campo de la hora sería equivalente a cada hora o un
                asterisco en el campo del mes sería equivalente a cada mes.

Ejemplo:

Programe un cron para que se ejecute en cada Sabado a las 7 pm.

0 19 * * 6 /scripts/job.sh

                Opcion 4) Si usted elige la opción 4 debe pasar el directorio o archivo de origen y destino que quiere
                          realizar backup ya sea localmente como a un host remoto."

echo "$instrucciones"
}

