#!/bin/bash
# Declarado el interprete de comandos


# VERIFICACION DE QUE SEA ROOT
echo "Bienvenido al Script de Backup"
sleep 1
echo ""
echo "AVISO: Este programa debe ejecutarse como root."
echo ""
echo "Analizando si es root.."

sleep 1s

if [ $UID -ne 0 ]; then
	echo "Ejecute este programa como 'root'"
	exit 1   # Se Sale del programa con un exit 'status' 1
fi
echo ""
echo "Muchas gracias por ejecutar este script como 'root', ahora continuaremos con la ejecución."
echo ""
sleep 1
echo "Parte 1.."
sleep 1

#-----------------Primera parte----------------------------
echo ""

read -e -p "Por favor ingresar el fichero que desea localizar:" FICHERO

if [ -f "$FICHERO" ]
then
   echo "El fichero ${FICHERO} existe"
else
   echo "El fichero ${FICHERO} no existe"
   sleep 1
   echo "Por favor vuelva a ejecutar el script nuevamente con un fichero valido"
   sleep 1
   exit 1   
fi
	
sleep 2
echo ""
echo " Parte 2.."
sleep 2
echo ""
#-----------------Segunda Parte----------------------------
if [ -x "$FICHERO"  ]
then
        echo "El fichero $FICHERO tiene permisos de ejecucíón."
        sleep 1 
	echo ""
	read -p "Desea visualizar la información del fichero?[y/n]:" ANS


	if [[ "$ANS" == [yY] ]]; then
      
		echo "Veamos la información del fichero"
      		sleep 3
   		REP="ls -lt"
      		$REP $FICHERO
		echo " El fichero tiene permisos de ejecución!!"
		echo ""	
      		read -p "Desea ejecutarlo? [y/n]: " RESP1
                
		elif [[ "$ANS" == [nN] ]]; then
		        echo " No se visualiza la información detallada del fichero"
	       		sleep 1
	 		echo "Se cerrara el script, por favor aceptar la visualización del fichero"
			sleep 1
		        exit 0
		else
		        echo "Opción incorrecta,solo permite ingresar y/n"
			sleep 1
	       		echo "Vuelva a ejecutar el script"
	 		sleep 1		
			exit 0 
	fi	

		if [[ "$RESP1" == [yY] ]]; then
  			echo " "
        		echo "Preparando la ejecución del fichero"
        		sleep 1
        		echo " "
        		echo -ne " Procesando la información (33%)\r"
        		sleep 1
			echo -ne " Finalizando el proceso   (100%)\r"
        		echo " "
        		echo -ne '\n'
        		sleep 1
        		echo " "
        		sleep 1
        		echo "Se esta ejecutando el fichero $FICHERO"
			echo""
			sleep 2
			echo "Fichero ejecutado,gracias por todo"
			sleep 1
			exit 0	
	else
	
		sleep 1
		echo "No se a ejecutado el fichero"
		sleep 1
		echo "Muchas gracias por ejecutar este script, hasta la proxima!!"
		exit 0
		fi
           
	fi


	if [ -r "$FICHERO" ] 
	then
	echo "El fichero $FICHERO no tiene permisos de ejecución,pero somos dueños"
	read -p "Desea otorgarle permisos de ejecución al fichero?[y/n]:" RESP2

        	if [[ "$RESP2" == [yY] ]]; then

                	echo ""
                	EJC= chmod +x $FICHERO
                	echo "Se le otorgaron los permisos al fichero, a continuación se imprimira el resultado"
                	sleep 2
                	$EJC
                	ls -l $FICHERO
                	sleep 1

		elif [[ "$RESP2" == [nN] ]]; then
        		echo " El fichero quedo sin permisos"
                        sleep 1
                        echo "Si desea que el fichero tenga permisos vuelva a ejecutar el script,saludos."
                        exit 0

		
		else
                	echo "Opción incorrecta,solo permite ingresar y/n"
                        sleep 1
			echo "Vuelva a ejecutar el script"
                        sleep 1
                        exit 0

	
		fi 
        fi

	






            

