#Definimos interprete de comando
#!/bin/bash

# Limpiamos la pantalla 
clear

#Argumento
args=( $@ )

   
#Valida si no ingresaste ningun opcion
if [[ -z "$args" ]]; then

	
	echo " No pasaste ninguna opción"
	sleep 1
        cat man.txt
	exit 1




else	

#Valida que el argumento sea una IP valida y luego recorre el for
if [[ ${args[-1]} =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9] ]] 2>/dev/null; then
       	
		
		

	for opcion in "${!args[@]}"; do
 		
		case ${args[$opcion]} in
		
			-C) cantidad=${args[$(($opcion+1))]}
				#Valida que el valor sea un numero entero positivo
			    if [[ $cantidad =~ ^[1-9]+[0-9]*$ ]]; then
    				counter="-c $cantidad"
    		
			    else
				 clear
    				 echo "Usted debe ingresar un numero entero positivo"
    				 cat man.txt
				 exit 1
			
    				fi

				;;
		
			-T) timestamp="-D"
	
				;;
		
			-p) 	proto=${args[$(($opcion+1))]}
			        #Valida que el valor ingresado sea 4 0 6
				if [ $proto -eq 4 -o $proto -eq 6 ]; then
    				p="-$proto"
   			
				else
					clear	
   					echo "Usted puede ingresar unicamente las opciones 4 o 6 para el protocolo de Internet."
   					cat man.txt
					exit 1
				
   				fi
			;;

			-b) b="-b"
			;;
				
		esac
	done

  ping $b $timestamp $p $counter ${args[-1]}
	
	#Valida que el argumento sea un hostname valido
	elif [[ ${args[-1]} =~ ^[A-Za-z]+\.[A-Za-z]+ ]] 2>/dev/null; then


		for opcion in "${!args[@]}"; do
        		
			case ${args[$opcion]} in

                	-C) cantidad=${args[$(($opcion+1))]}
                		if [[ $cantidad =~ ^[1-9]+[0-9]*$ ]]; then
                       	 	counter="-c $cantidad"
               
		 		else
                        		clear
                        		echo "Usted debe ingresar un numero entero positivo"
                       			cat man.txt
                        		exit 1

                        	fi

                	;;

                	-T) timestamp="-D"

                	;;

                	-p)     proto=${args[$(($opcion+1))]}

                        	if [ $proto -eq 4 -o $proto -eq 6 ]; then
                                p="-$proto"
                        	
			
				else
                                	clear
                                	echo "Usted puede ingresar unicamente las opciones 4 o 6 para el protocolo de Internet."
                                	cat man.txt
                                	exit 1

                        	fi
                	;;

                	-b) b="-b"
                	;;
        		

			esac
		done

	
 ping $b $timestamp $p $counter ${args[-1]}



else
	echo "La IP o el hostname ingresado es erroneo"
	sleep 1
	cat man.txt
	

fi

fi











